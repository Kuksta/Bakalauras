#libraries
library(calibrate)
library(xtable)
library(data.table)
library(plyr)
library(dplyr)
library(tidyr)
library(ggplot2)

# Data manipulation ----
fread("data/data_fixed.csv")->data
data->data1
# Modelling -----
# |----- OLS ------

data1$Comm %>% unique %>% as.numeric->prekes
lapply(prekes, function(i){
  lapply(prekes, function(x){
    data1 %>% filter(Comm==i) %>% select(-Comm) %>% full_join(
      data1 %>% filter(Comm==x) %>% select(-Comm), by = c("Year", "Month", "Store")) %>%
      select(Year, Month, Volume.x, Volume.y, Price.x, Price.y)->a
    names(a)<-c("Year", "Month", paste0("Volume",i), paste0("Volume",x), paste0("Price",i), paste0("Price",x))
    paste0("log(Volume",i,"/Volume",x,")~log(Price",x,"/Price",i,")") %>% as.formula->form
    lm(form, na.omit(a))->mod
  })
})->all.mods.ols
# |-------- Extracting values ------
lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    all.mods.ols[[z]][[x]]$coefficients[2]->el
    # all.mods.ols[[z]][[x]]$coefficients[1]->el
  })
})->elast.ols
rbindlist(elast.ols)->elasticity.ols

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    all.mods.ols[[z]][[x]]$coefficients[1]->el
  })
})->interc.ols
rbindlist(interc.ols)->intercept.ols

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    if (length(summary(all.mods.ols[[z]][[x]])$coefficients[,4])==1){
    # if (length(summary(all.mods.ols[[z]][[x]])$coefficients[,4])==0){
      py <- NA
    } else {
      summary(all.mods.ols[[z]][[x]])$coefficients[2,4]->py
      # summary(all.mods.ols[[z]][[x]])$coefficients[1,4]->py
      
    }
  })
})->p_value.ols
rbindlist(p_value.ols)->p_value.ols

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
      summary(all.mods.ols[[z]][[x]])$adj.r.squared->r
  })
})->adj_rsq.ols
rbindlist(adj_rsq.ols)->adj_r_squared.ols

names(elasticity.ols)<-as.character(prekes)
names(intercept.ols)<-as.character(prekes)
names(p_value.ols) <-as.character(prekes)
names(adj_r_squared.ols) <-as.character(prekes)
elasticity.ols$Comm1<-as.character(prekes)
intercept.ols$Comm1<-as.character(prekes)
p_value.ols$Comm1<-as.character(prekes)
adj_r_squared.ols$Comm1<-as.character(prekes)
elasticity.ols %>% gather("Comm2", "Elasticity", 1:length(prekes)) %>% 
  full_join(intercept.ols %>% gather("Comm2", "Intercept", 1:length(prekes)), by = c("Comm1", "Comm2")) %>% 
  full_join(p_value.ols %>% gather("Comm2", "p-value", 1:length(prekes)), by = c("Comm1", "Comm2")) %>% 
  full_join(adj_r_squared.ols %>% gather("Comm2", "R-squared(adj)", 1:length(prekes)), by = c("Comm1", "Comm2"))->final.results.ols
round(final.results.ols[,c(3,4)], digits = 8 )->final.results.ols[,c(3,4)]
round(final.results.ols[,c(5,6)], digits = 3 )->final.results.ols[,c(5,6)]
# round(final.results.ols[,c(3)], digits = 8 )->final.results.ols[,c(3)]
# round(final.results.ols[,c(4,5)], digits = 3 )->final.results.ols[,c(4,5)]
# |----- PAM ------
lapply(prekes, function(i){
  lapply(prekes, function(x){ 
    data1 %>% filter(Comm==i) %>% select(-Comm) %>% full_join(
      data1 %>% filter(Comm==x) %>% select(-Comm), by = c("Year", "Month", "Store")) %>%
      select(Year, Month, Volume.x, Volume.y, Price.x, Price.y)->a
    names(a)<-c("Year", "Month", paste0("Volume",i), paste0("Volume",x), paste0("Price",i), paste0("Price",x))
    na.omit(a)->a
    paste0("log(Volume",i,"/Volume",x,")~log(Price",x,"/Price",i,")+log(lag(Volume",i,")/lag(Volume",x,"))") %>% as.formula->form
    if(length(cbind(lag(a[,3]), lag(a[,4]))[,1])==1){ 
      mod<-list()
      mod$coefficients <- c(NA, NA)
      mod
    }  else {
    lm(form, a)->mod
    }  
  }) 
})->all.mods.pols
# |-------- Extracting values ------
lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    all.mods.pols[[z]][[x]]$coefficients[2]->el
  })
})->elast.pols
rbindlist(elast.pols)->elasticity.pols

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    all.mods.pols[[z]][[x]]$coefficients[1]->el
  })
})->interc.pols
rbindlist(interc.pols)->intercept.pols

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    if(all.mods.pols[[z]][[x]] %>% length()==1){
      py<-NA
    } else {
    if (length(summary(all.mods.pols[[z]][[x]])$coefficients[,4])==1){
      py <- NA
    } else {
      summary(all.mods.pols[[z]][[x]])$coefficients[2,4]->py
    }}
  })
})->p_value.pols
rbindlist(p_value.pols)->p_value.pols

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    if (all.mods.pols[[z]][[x]] %>% length()==1){
    r<- NA
    } else {
      summary(all.mods.pols[[z]][[x]])$adj.r.squared->r
    }
  })
})->adj_rsq.pols
rbindlist(adj_rsq.pols)->adj_r_squared.pols

names(elasticity.pols)<-as.character(prekes)
names(intercept.pols)<-as.character(prekes)
names(p_value.pols) <-as.character(prekes)
names(adj_r_squared.pols) <-as.character(prekes)
elasticity.pols$Comm1<-as.character(prekes)
intercept.pols$Comm1<-as.character(prekes)
p_value.pols$Comm1<-as.character(prekes)
adj_r_squared.pols$Comm1<-as.character(prekes)
elasticity.pols %>% gather("Comm2", "Elasticity", 1:length(prekes)) %>% 
  full_join(intercept.pols %>% gather("Comm2", "Intercept", 1:length(prekes)), by = c("Comm1", "Comm2")) %>% 
  full_join(p_value.pols %>% gather("Comm2", "p-value", 1:length(prekes)), by = c("Comm1", "Comm2")) %>% 
  full_join(adj_r_squared.pols %>% gather("Comm2", "R-squared(adj)", 1:length(prekes)), by = c("Comm1", "Comm2"))->final.results.pols
round(final.results.pols[,c(3,4)], digits = 8 )->final.results.pols[,c(3,4)]
round(final.results.pols[,c(5,6)], digits = 3 )->final.results.pols[,c(5,6)]
# |----- ECM ------
lapply(prekes, function(i){
  lapply(prekes, function(x){
    data1 %>% filter(Comm==i) %>% select(-Comm) %>% full_join(
      data1 %>% filter(Comm==x) %>% select(-Comm), by = c("Year", "Month", "Store")) %>%
      select(Year, Month, Volume.x, Volume.y, Price.x, Price.y)->a
    names(a)<-c("Year", "Month", paste0("Volume",i), paste0("Volume",x), paste0("Price",i), paste0("Price",x))
    na.omit(a)->a
    paste0("diff(log(a$Volume",i,"/a$Volume",x,"))~diff(log(a$Price",x,"/a$Price",i,"))+
           (log(lag(a$Volume",i,")[-1]/lag(a$Volume",x,")[-1])-log(lag(a$Price",x,")[-1]/lag(a$Price",i,")[-1]))") %>% as.formula->form
    if(length(cbind(log(lag(a[,3])), log(lag(a[,4])))[,1])==1){ 
      mod<-list()
      mod$coefficients <- c(NA, NA)
      mod
    }  else {
      lm(form, a)->mod
    }  
  })
})->all.mods.ecm
# |-------- Extracting values ------
lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    all.mods.ecm[[z]][[x]]$coefficients[2]->el
  })
})->elast.ecm
rbindlist(elast.ecm)->elasticity.ecm

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    all.mods.ecm[[z]][[x]]$coefficients[1]->el
  })
})->interc.ecm
rbindlist(interc.ecm)->intercept.ecm

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    if(all.mods.ecm[[z]][[x]] %>% length()==1){
      py<-NA
    } else {
      if (length(summary(all.mods.ecm[[z]][[x]])$coefficients[,4])==1){
        py <- NA
      } else {
        summary(all.mods.ecm[[z]][[x]])$coefficients[2,4]->py
      }}
  })
})->p_value.ecm
rbindlist(p_value.ecm)->p_value.ecm

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    if (all.mods.ecm[[z]][[x]] %>% length()==1){
      r<- NA
    } else {
      summary(all.mods.ecm[[z]][[x]])$adj.r.squared->r
    }
  })
})->adj_rsq.ecm
rbindlist(adj_rsq.ecm)->adj_r_squared.ecm

names(elasticity.ecm)<-as.character(prekes)
names(intercept.ecm)<-as.character(prekes)
names(p_value.ecm)<-as.character(prekes)
names(adj_r_squared.ecm)<-as.character(prekes)
elasticity.ecm$Comm1<-as.character(prekes)
intercept.ecm$Comm1<-as.character(prekes)
p_value.ecm$Comm1<-as.character(prekes)
adj_r_squared.ecm$Comm1<-as.character(prekes)
elasticity.ecm %>% gather("Comm2", "Elasticity", 1:length(prekes)) %>% 
  full_join(intercept.ecm %>% gather("Comm2", "Intercept", 1:length(prekes)), by = c("Comm1", "Comm2")) %>% 
  full_join(p_value.ecm %>% gather("Comm2", "p-value", 1:length(prekes)), by = c("Comm1", "Comm2")) %>% 
  full_join(adj_r_squared.ecm %>% gather("Comm2", "R-squared(adj)", 1:length(prekes)), by = c("Comm1", "Comm2"))->final.results.ecm
round(final.results.ecm[,c(3,4)], digits = 8 )->final.results.ecm[,c(3,4)]
round(final.results.ecm[,c(5,6)], digits = 3 )->final.results.ecm[,c(5,6)]

# Rezultatu vaizdavimas -------
# Plots

pdf("plots/OLS.pdf", width=10, height = 7, family  ="mono")
for (i in prekes) {
plot(x = 1:length(prekes), y = final.results.ols %>%filter(Comm1==i) %>% .$Elasticity, ylab = "Elasticity", xlab = "", xaxt="n")
text(x = 1:length(prekes), y = final.results.ols %>%filter(Comm1==i) %>% .$Elasticity,
     final.results.ols %>%filter(Comm1==i) %>% .$Comm2 , cex=0.8, pos =3)
lines(y=rep(0,length(prekes)), x= 1:length(prekes), col = "red")
mtext(i, side = 3, line = 1)
}
dev.off()

pdf("plots/POLS.pdf", width=10, height = 7, family  ="mono")
for (i in prekes) {
  plot(x = 1:length(prekes), y = final.results.pols %>%filter(Comm1==i) %>% .$Elasticity, ylab = "Elasticity", xlab = "", xaxt="n")
  text(x = 1:length(prekes), y = final.results.pols %>%filter(Comm1==i) %>% .$Elasticity,
       final.results.pols %>%filter(Comm1==i) %>% .$Comm2 , cex=0.8, pos =3)
  lines(y=rep(0,length(prekes)), x= 1:length(prekes), col = "red")
  mtext(i, side = 3, line = 1)
}
dev.off()

pdf("plots/ECM.pdf", width=10, height = 7, family  ="mono")
for (i in prekes) {
  plot(x = 1:length(prekes), y = final.results.ecm %>%filter(Comm1==i) %>% .$Elasticity, ylab = "Elasticity", xlab = "", xaxt="n")
  text(x = 1:length(prekes), y = final.results.ecm %>%filter(Comm1==i) %>% .$Elasticity,
       final.results.ecm %>%filter(Comm1==i) %>% .$Comm2 , cex=0.8, pos =3)
  lines(y=rep(0,length(prekes)), x= 1:length(prekes), col = "red")
  mtext(i, side = 3, line = 1)
}
dev.off()

# Lentos
for (i in 101:118) {
final.results.ols %>%filter(Comm1==i) %>% .[,-1] %>% full_join(
  final.results.pols %>% filter(Comm1==i) %>% .[,-1], by = "Comm2") %>% full_join(
    final.results.ecm %>% filter(Comm1==i) %>% .[,-1], by = "Comm2") ->a
names(a)<-c("Comm", "Elasticity_OLS", "Intercept_OLS","P-Value_OLS","R-squared(adj)_OLS",
            "Elasticity_POLS", "Intercept_POLS","P-Value_POLS","R-squared(adj)_pOLS",
            "Elasticity_ECM", "Intercept_ECM","P-Value_ECM","R-squared(adj)_ECM")
xtable(a, caption = i, digits = 4)->x.a
print(x.a, include.rownames=FALSE, file = paste0("L",i,".txt"))
}

elasticity.ols %>% select(Comm1, `101`:`118`) %>% xtable(., caption = "OLS", digits = 4)->ols.t
print(ols.t, include.rownames = F, file = "ols.txt")
p_value.ols %>% select(Comm1, `101`:`118`) %>% xtable(., caption = "OLS", digits = 4)->ols.p
print(ols.p, include.rownames = F, file = "ols_pval.txt")

final.results.ols[final.results.ols$Comm1<final.results.ols$Comm2,] %>% arrange(Comm1) %>%
  unite(Comm,1:2, sep = "/") %>% .[1:76,] ->table11
final.results.ols[final.results.ols$Comm1<final.results.ols$Comm2,] %>% arrange(Comm1) %>%
  unite(Comm,1:2, sep = "/") %>% .[77:153,] ->table12
xtable(table11) %>% print(include.rownames=F)
xtable(table12) %>% print(include.rownames=F)

final.results.pols[final.results.pols$Comm1<final.results.pols$Comm2,] %>% arrange(Comm1) %>%
  unite(Comm,1:2, sep = "/") %>% .[1:76,] ->table21
final.results.pols[final.results.pols$Comm1<final.results.pols$Comm2,] %>% arrange(Comm1) %>%
  unite(Comm,1:2, sep = "/") %>% .[77:153,] ->table22
xtable(table21) %>% print(include.rownames=F)
xtable(table22) %>% print(include.rownames=F)

final.results.ecm[final.results.ecm$Comm1<final.results.ecm$Comm2,] %>% arrange(Comm1) %>%
  unite(Comm,1:2, sep = "/") %>% .[1:76,] ->table31
final.results.ecm[final.results.ecm$Comm1<final.results.ecm$Comm2,] %>% arrange(Comm1) %>%
  unite(Comm,1:2, sep = "/") %>% .[77:153,] ->table32
xtable(table31) %>% print(include.rownames=F)
xtable(table32) %>% print(include.rownames=F)

#|------ Bendri grafikai ---------
legendary2<-function(legend, col, line=0,side=3,adj=1, cex=0.8,presymbol = "-",...){
  
  legend <- paste(presymbol,legend)
  olegend <- legend
  
  for(i in 1:length(olegend)) {
    legend <- paste(legend[i:length(olegend)],collapse = "  ")
    mtext(line=line,side=side,text = legend,col=col[i],adj=adj,cex=cex,...)
    legend <- olegend
  }
  
}

# |-----|----- Graph1 OLS ------
final.results.ols[which(final.results.ols$`p-value`=="NaN"),]$`p-value`<-0
final.results.ols[which(final.results.ols$`R-squared(adj)`=="NaN"),]$`R-squared(adj)`<-0
pdf("plots/resols.pdf", width=10, height = 7, family = "mono", encoding = "WinAnsi.enc")
par(new = F)
for(i in prekes){
plot(y =filter(final.results.ols, Comm1==i) %>% .$Elasticity, x = rep(i, length(prekes)),xaxt = "n", yaxt = "n" ,
     type = "l", xlim = c(101,last(prekes)), 
     ylim = c(-0.4,final.results.ols$Elasticity %>% max(., na.rm = T)+0.1),
     xlab = "", ylab = "")
     par(new = T)
}
plot(y = c(0.6273918,0.6966606), x = c(102,102), xaxt = "n", yaxt = "n", xlim = c(first(prekes), last(prekes)),
     ylim = c(-0.4,final.results.ols$Elasticity %>% max(., na.rm = T)+0.1),
     type = "l", xlab ="", ylab = "")
for(i in prekes){
  min3 <- setorder(final.results.ols %>% filter(Comm1==i) %>% na.omit, Elasticity)[1:3,]
  max3 <- setorder(final.results.ols %>% filter(Comm1==i) %>% na.omit, -Elasticity)[1:3,]
  textxy(rep(i,3), c(max3$Elasticity), c(max3$Comm2), pos = c(4,2,4))
  textxy(rep(i,3), c(min3$Elasticity), c(min3$Comm2),pos = c(4,2,4))
  
points(y = sort(filter(final.results.ols, Comm1==i) %>% .$Elasticity, F)[1:3], x = rep(i,3), pch= 19)
points(y = sort(filter(final.results.ols, Comm1==i) %>% .$Elasticity, T)[1:3], x = rep(i,3), pch = 21)
}
axis(1, at=c(101:last(prekes)),labels=prekes,
     col.axis="black", las=1, cex.axis=0.7, tck=-.02)
axis(2, at=seq(-0.4,1,0.4),
     labels=seq(-0.4,1,0.4),
     col.axis="black", las = 1, cex.axis=0.7, tck=-.02)
lines(y=rep(0,length(prekes)), x = 101:last(prekes), lty =2)
legendary2(c("Didžiausios reikšmės", "Mažiausios reikšmės"), presymbol = c(" "," "), col = "black")
par(xpd=TRUE)
par(new=T)
plot(x = c(111,115.1), y = c(1.081,1.081), xlim = c(101,118), ylim = c(-0.4,1),
     type = "p", pch = c(21,19), xaxt = "n", yaxt = "n", xlab = "Prekės", ylab = "Elastingumas")
mtext(line = 2, side = 3, text = "Mažiausių kvadratų metodas")
dev.off()
# |*** Aprasymui-----
final.results.ols->sum.ols
# p-value
sum.ols[which(sum.ols$`p-value`<=0.05),] %>% nrow/2->pi
# r-squared
sum.ols[which(sum.ols$`p-value`<=0.05&sum.ols$`R-squared(adj)`!=0),]$`R-squared(adj)` %>% min
sum.ols[which(sum.ols$`p-value`<=0.05&sum.ols$`R-squared(adj)`<=0.3),] %>% nrow/2/153*100
# <0
sum.ols[which(sum.ols$Elasticity<0&
                sum.ols$`p-value`<0.05),] %>% nrow/2->t0
t0
# ## 0-0.3
sum.ols[which(sum.ols$Elasticity>=0&sum.ols$Elasticity<=0.3&
           sum.ols$`p-value`<0.05),] %>% nrow/2->t0.3
t0.3
# ## 0.3-0.5
sum.ols[which(sum.ols$Elasticity>=0&sum.ols$Elasticity>0.3&
          sum.ols$`p-value`<0.05&sum.ols$Elasticity<=0.5),] %>% nrow/2->t0.5
t0.5
# 0-5-0.8
sum.ols[which(sum.ols$Elasticity>=0&sum.ols$Elasticity>0.5&
                sum.ols$`p-value`<0.05&sum.ols$Elasticity<=0.8),] %>% nrow/2->t0.8
t0.8
sum.ols[which(sum.ols$Elasticity>=0&sum.ols$Elasticity>0.8&
                sum.ols$`p-value`<0.05&sum.ols$Elasticity<=1),] %>% nrow/2->t1
t1
sum.ols[which(sum.ols$Elasticity>=0&sum.ols$Elasticity>1&
                sum.ols$`p-value`<0.05),] %>% nrow/2->t2
t2
data.frame(t0,t0.3,t0.5,t0.8,t1,t2) %>% as.data.frame->sum
names(sum)<-c("<0","0-0.3", "0.31-0.5", "0.51-0.8","0.81-1", ">1")
sum
print(xtable(sum, digits = 0), include.rownames=F)
# |-----|----- Graph1 PAM ------
final.results.pols[which(final.results.pols$`p-value`=="NaN"),]$`p-value`<-0
final.results.pols[which(final.results.pols$`R-squared(adj)`=="NaN"),]$`R-squared(adj)`<-0
pdf("plots/respam.pdf", width=10, height = 7, family  ="mono",encoding = "WinAnsi.enc")
par(new = F)
for(i in prekes){
  plot(y =filter(final.results.pols, Comm1==i) %>% .$Elasticity, x = rep(i, 18),xaxt = "n", yaxt = "n",
       type = "l", xlim = c(first(prekes),last(prekes)), ylim = c(final.results.pols$Elasticity %>% min(., na.rm = T)-0.1,final.results.pols$Elasticity %>% max(., na.rm = T)+0.1),
       xlab = "", ylab = "")
  par(new = T)
}
plot(y = c(0.6319579,0.6966242), x = c(102,102), xaxt = "n", yaxt = "n", xlim = c(first(prekes), last(prekes)),
     ylim = c(final.results.pols$Elasticity %>% min(., na.rm = T)-0.1,final.results.pols$Elasticity %>% max(., na.rm = T)+0.1),
     type = "l", xlab ="", ylab = "")
for(i in prekes){
  min3 <- setorder(final.results.pols %>% filter(Comm1==i) %>% na.omit, Elasticity)[1:3,]
  max3 <- setorder(final.results.pols %>% filter(Comm1==i) %>% na.omit, -Elasticity)[1:3,]
  textxy(rep(i,3), c(max3$Elasticity), c(max3$Comm2), pos = c(4,2,4))
  textxy(rep(i,3), c(min3$Elasticity), c(min3$Comm2), pos = c(4,2,4))
  
  points(y = sort(filter(final.results.pols, Comm1==i) %>% .$Elasticity, F)[1:3], x = rep(i,3), pch = 19 )
  points(y = sort(filter(final.results.pols, Comm1==i) %>% .$Elasticity, T)[1:3], x = rep(i,3), pch = 21 )
}
axis(1, at=c(101:last(prekes)),labels=prekes,
     col.axis="black", las=1, cex.axis=0.7, tck=-.02)
axis(2, at=seq(0,1, 0.4),
     labels=seq(0,1,0.4),
     col.axis="black", las = 1, cex.axis=0.7, tck=-.02)
lines(y=rep(0,length(prekes)), x = 101:last(prekes), lty =2)
legendary2(c("Didžiausios reikšmės", "Mažiausios reikšmės"), presymbol = c(" "," "), col = "black")
par(xpd=TRUE)
par(new=T)
plot(x = c(111,115.1), y = c(1.081,1.081), xlim = c(101,118), ylim = c(-0.4,1),
     type = "p", pch = c(21,19), xaxt = "n", yaxt = "n", xlab = "Prekės", ylab = "Elastingumas")
mtext(line = 2, side = 3, text = "Dalinio reguliavimo modelis")
dev.off()
# |*** Aprasymui-----
final.results.pols->sum.ols
# p-value
sum.ols[which(sum.ols$`p-value`>0.05),] %>% nrow/2->pi
# r-squared
sum.ols[which(sum.ols$`p-value`<=0.05&sum.ols$`R-squared(adj)`!=0),]$`R-squared(adj)` %>% min
sum.ols[which(sum.ols$`p-value`<=0.05&sum.ols$`R-squared(adj)`<=0.3),] %>% nrow/2/153*100
# <0
sum.ols[which(sum.ols$Elasticity<0&
                sum.ols$`p-value`<0.05),] %>% nrow/2->t0
t0
# ## 0-0.3
sum.ols[which(sum.ols$Elasticity>=0&sum.ols$Elasticity<=0.3&
                sum.ols$`p-value`<0.05),] %>% nrow/2->t0.3
t0.3
# ## 0.3-0.5
sum.ols[which(sum.ols$Elasticity>=0&sum.ols$Elasticity>0.3&
                sum.ols$`p-value`<0.05&sum.ols$Elasticity<=0.5),] %>% nrow/2->t0.5
t0.5
# 0-5-0.8
sum.ols[which(sum.ols$Elasticity>=0&sum.ols$Elasticity>0.5&
                sum.ols$`p-value`<0.05&sum.ols$Elasticity<=0.8),] %>% nrow/2->t0.8
t0.8
sum.ols[which(sum.ols$Elasticity>=0&sum.ols$Elasticity>0.8&
                sum.ols$`p-value`<0.05&sum.ols$Elasticity<=1),] %>% nrow/2->t1
t1
sum.ols[which(sum.ols$Elasticity>=0&sum.ols$Elasticity>1&
                sum.ols$`p-value`<0.05),] %>% nrow/2->t2
t2
data.frame(t0,t0.3,t0.5,t0.8,t1,t2) %>% as.data.frame->sum
names(sum)<-c("<0","0-0.3", "0.31-0.5", "0.51-0.8","0.81-1", ">1")
sum
print(xtable(sum, digits = 0), include.rownames=F)
# |-----|----- Graph1 ECM ------
final.results.ecm[which(final.results.ecm$`p-value`=="NaN"),]$`p-value`<-0
final.results.ecm[which(final.results.ecm$`R-squared(adj)`=="NaN"),]$`R-squared(adj)`<-0
pdf("plots/resecm.pdf", width=10, height = 7, family  ="mono",encoding = "WinAnsi.enc")
par(new = F)
for(i in prekes){
  plot(y =filter(final.results.ecm, Comm1==i) %>% .$Elasticity, x = rep(i, 18),xaxt = "n", yaxt = "n" ,type = "l",
       xlim = c(first(prekes),last(prekes)), ylim = c(final.results.ecm$Elasticity %>% min(., na.rm = T)-0.1,final.results.ecm$Elasticity %>% max(., na.rm = T)+0.1),
       xlab = "", ylab = "")
  par(new = T)
}
plot(y = c(0.3443158,0.5701398), x = c(113,113), xaxt = "n", yaxt = "n", xlim = c(first(prekes), last(prekes)),
     ylim = c(final.results.ecm$Elasticity %>% min(., na.rm = T)-0.1,final.results.ecm$Elasticity %>% max(., na.rm = T)+0.1),
     type = "l", xlab ="", ylab = "")
par(new=T)
plot(y = c(0.4925015,0.5525354), x = c(102,102), xaxt = "n", yaxt = "n", xlim = c(first(prekes), last(prekes)),
     ylim = c(final.results.ecm$Elasticity %>% min(., na.rm = T)-0.1,final.results.ecm$Elasticity %>% max(., na.rm = T)+0.1),
     type = "l", xlab ="", ylab = "")

for(i in prekes){
  min3 <- setorder(final.results.ecm %>% filter(Comm1==i) %>% na.omit, Elasticity)[1:3,]
  max3 <- setorder(final.results.ecm %>% filter(Comm1==i) %>% na.omit, -Elasticity)[1:3,]
  textxy(rep(i,3), c(max3$Elasticity), c(max3$Comm2), pos = c(4,2,4))
  textxy(rep(i,3), c(min3$Elasticity), c(min3$Comm2), pos = c(4,2,4))
  
  points(y = sort(filter(final.results.ecm, Comm1==i) %>% .$Elasticity, F)[1:3], x = rep(i,3), pch = 19)
  points(y = sort(filter(final.results.ecm, Comm1==i) %>% .$Elasticity, T)[1:3], x = rep(i,3), pch = 21)
}
axis(1, at=c(101:last(prekes)),labels=prekes,
     col.axis="black", las=1, cex.axis=0.7, tck=-.02)
axis(2, at=seq(0,1,0.4),
     labels=seq(0,1,0.4),
     col.axis="black", las = 1, cex.axis=0.7, tck=-.02)
lines(y=rep(0,length(prekes)), x = 101:last(prekes), lty =2)
legendary2(c("Didžiausios reikšmės", "Mažiausios reikšmės"), presymbol = c(" "," "), col = "black")
par(xpd=TRUE)
par(new=T)
plot(x = c(111,115.1), y = c(1.081,1.081), xlim = c(101,118), ylim = c(-0.4,1),
     type = "p", pch = c(21,19), xaxt = "n", yaxt = "n", xlab = "Prekės", ylab = "Elastingumas")
mtext(line = 2, side = 3, text = "Paklaidų korekcijos modelis")
dev.off()
# |*** Aprasymui -------
final.results.ecm->sum.ols
# p-value
sum.ols[which(sum.ols$`p-value`>0.05),] %>% nrow/2->pi
# r-squared
sum.ols[which(sum.ols$`p-value`<=0.05&sum.ols$`R-squared(adj)`!=0),]$`R-squared(adj)` %>% max
sum.ols[which(sum.ols$`p-value`<=0.05&sum.ols$`R-squared(adj)`<=0.3),] %>% nrow/2/153*100
# <0
sum.ols[which(sum.ols$Elasticity<0&
                sum.ols$`p-value`<0.05),] %>% nrow/2->t0
t0
# ## 0-0.3
sum.ols[which(sum.ols$Elasticity>=0&sum.ols$Elasticity<=0.3&
                sum.ols$`p-value`<0.05),] %>% nrow/2->t0.3
t0.3
# ## 0.3-0.5
sum.ols[which(sum.ols$Elasticity>=0&sum.ols$Elasticity>0.3&
                sum.ols$`p-value`<0.05&sum.ols$Elasticity<=0.5),] %>% nrow/2->t0.5
t0.5
# 0-5-0.8
sum.ols[which(sum.ols$Elasticity>=0&sum.ols$Elasticity>0.5&
                sum.ols$`p-value`<0.05&sum.ols$Elasticity<=0.8),] %>% nrow/2->t0.8
t0.8
sum.ols[which(sum.ols$Elasticity>=0&sum.ols$Elasticity>0.8&
                sum.ols$`p-value`<0.05&sum.ols$Elasticity<=1),] %>% nrow/2->t1
t1
sum.ols[which(sum.ols$Elasticity>=0&sum.ols$Elasticity>1&
                sum.ols$`p-value`<0.05),] %>% nrow/2->t2
t2
data.frame(t0,t0.3,t0.5,t0.8,t1,t2) %>% as.data.frame->sum
names(sum)<-c("<0","0-0.3", "0.31-0.5", "0.51-0.8","0.81-1", ">1")
sum
print(xtable(sum, digits = 0), include.rownames=F)

# Metodų palyginimas ----
library(forecast)
# |---- OLS ----
lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    accuracy(all.mods.ols[[z]][[x]])[,c(2)]->rmse
  })
})->rmse
rbindlist(rmse)->rmse.kof

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    accuracy(all.mods.ols[[z]][[x]])[,c(6)]->rmse
  })
})->mase
rbindlist(mase)->mase.kof
names(rmse.kof) <-as.character(prekes)
rmse.kof$Comm1<-as.character(prekes)
names(mase.kof) <-as.character(prekes)
mase.kof$Comm1<-as.character(prekes)
mase.kof %>% gather("Comm2", "Value", 1:18)->mase.kof
rmse.kof %>% gather("Comm2", "Value", 1:18)->rmse.kof
mase.kof[mase.kof$Comm1!=mase.kof$Comm2,]->mase.kof
rmse.kof[rmse.kof$Comm1!=rmse.kof$Comm2,]->rmse.kof
mase.kof[mase.kof$Comm1<mase.kof$Comm2,]->mase.kof
rmse.kof[rmse.kof$Comm1<rmse.kof$Comm2,]->rmse.kof
mase.kof[mase.kof$Value=="NaN",]$Value<-NA
rmse.kof[rmse.kof$Value=="NaN",]$Value<-NA
mean(mase.kof$Value, na.rm = T) # 0.6736858
mean(rmse.kof$Value) #1.037793

# |---- PAM ----
lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    if (all.mods.pols[[z]][[x]] %>% length==1){
    NA->rmse
    } else{
      accuracy(all.mods.pols[[z]][[x]])[,c(2)]->rmse
    }
  })
})->rmse
rbindlist(rmse)->rmse.kof

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    if (all.mods.pols[[z]][[x]] %>% length==1){
      NA->rmse
    } else{
    accuracy(all.mods.pols[[z]][[x]])[,c(6)]->rmse
    }
  })
})->mase
rbindlist(mase)->mase.kof
names(rmse.kof) <-as.character(prekes)
rmse.kof$Comm1<-as.character(prekes)
names(mase.kof) <-as.character(prekes)
mase.kof$Comm1<-as.character(prekes)
mase.kof %>% gather("Comm2", "Value", 1:18)->mase.kof
rmse.kof %>% gather("Comm2", "Value", 1:18)->rmse.kof
mase.kof[mase.kof$Comm1!=mase.kof$Comm2,]->mase.kof
rmse.kof[rmse.kof$Comm1!=rmse.kof$Comm2,]->rmse.kof
mase.kof[mase.kof$Comm1<mase.kof$Comm2,]->mase.kof
rmse.kof[rmse.kof$Comm1<rmse.kof$Comm2,]->rmse.kof
mase.kof[mase.kof$Value=="NaN",]$Value<-NA
rmse.kof[rmse.kof$Value=="NaN",]$Value<-NA
mean(mase.kof$Value, na.rm = T) # 0.6658879
mean(rmse.kof$Value, na.rm = T) # 1.032229

# |---- ECM ----
lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    if (all.mods.ecm[[z]][[x]] %>% length==1){
      NA->rmse
    } else{
      accuracy(all.mods.ecm[[z]][[x]])[,c(2)]->rmse
    }
  })
})->rmse
rbindlist(rmse)->rmse.kof

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    if (all.mods.ecm[[z]][[x]] %>% length==1){
      NA->rmse
    } else{
      accuracy(all.mods.ecm[[z]][[x]])[,c(6)]->rmse
    }
  })
})->mase
rbindlist(mase)->mase.kof
names(rmse.kof) <-as.character(prekes)
rmse.kof$Comm1<-as.character(prekes)
names(mase.kof) <-as.character(prekes)
mase.kof$Comm1<-as.character(prekes)
mase.kof %>% gather("Comm2", "Value", 1:18)->mase.kof
rmse.kof %>% gather("Comm2", "Value", 1:18)->rmse.kof
mase.kof[mase.kof$Comm1!=mase.kof$Comm2,]->mase.kof
rmse.kof[rmse.kof$Comm1!=rmse.kof$Comm2,]->rmse.kof
mase.kof[mase.kof$Comm1<mase.kof$Comm2,]->mase.kof
rmse.kof[rmse.kof$Comm1<rmse.kof$Comm2,]->rmse.kof
mase.kof[mase.kof$Value=="NaN",]$Value<-NA
rmse.kof[rmse.kof$Value=="NaN",]$Value<-NA
mean(mase.kof$Value, na.rm = T) # 0.5043881
mean(rmse.kof$Value, na.rm = T) # 1.143868

## ecm rez
final.results.ecm[final.results.ecm$Comm1<final.results.ecm$Comm2,] %>% unite(Comm,1:2, sep = "/") %>% select(Comm, Elasticity)  %>% arrange(-Elasticity)->ecmtable
ecmtable->ecmtable1
setorder(ecmtable1, -Elasticity, na.last = T)[1:10,] %>% 
  cbind(setorder(ecmtable1, Elasticity, na.last = T)[1:10,])->ecmtable1

ecmtable1 %>% xtable() %>% print(include.rownames = F)

# Liekanu testai -------
#|---- OLS ----
library(tseries)
lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
   all.mods.ols[[z]][[x]]$residuals->res
    Box.test(res)$p.value->bt
  })
})->box.ols
rbindlist(box.ols)->box.ols
names(box.ols) <-as.character(prekes)
box.ols$Comm1<-as.character(prekes)
box.ols %>% gather("Comm2", "Box", 1:18)->box.ols
box.ols[box.ols$Comm1<box.ols$Comm2,]->box.ols
box.ols$Box %>% round(digits = 3)->box.ols$Box

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    if (z==x){ adf<- NA}
    else{ all.mods.ols[[z]][[x]]$residuals->res
      if(length(res)>6){
    adf.test(res)$p.value->adf}
      else {
        adf<- NA
      }
    }
    })
})->adf.ols
rbindlist(adf.ols)->adf.1
names(adf.1) <-as.character(prekes)
adf.1$Comm1<-as.character(prekes)
adf.1 %>% gather("Comm2", "ADF", 1:18)->adf.1
adf.1[adf.1$Comm1<adf.1$Comm2,]->adf.1
adf.1$ADF %>% round(digits = 3)->adf.1$ADF

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    all.mods.ols[[z]][[x]]$residuals->res
    jarque.bera.test(res)$p.value->bt
  })
})->jbt.ols
rbindlist(jbt.ols)->jbt.ols
names(jbt.ols) <-as.character(prekes)
jbt.ols$Comm1<-as.character(prekes)
jbt.ols %>% gather("Comm2", "JBT", 1:18)->jbt.ols
jbt.ols[jbt.ols$Comm1<jbt.ols$Comm2,]->jbt.ols
jbt.ols$JBT %>% round(digits = 3)->jbt.ols$JBT

box.ols %>% full_join(jbt.ols, by = c("Comm1", "Comm2")) %>% 
  full_join(adf.1, by = c("Comm1", "Comm2"))->wntesting.ols
wntesting.ols[which(wntesting.ols$Box>=0.05),] %>% nrow->b.ols
wntesting.ols[which(wntesting.ols$ADF<=0.05),] %>% nrow->a.ols
wntesting.ols[which(wntesting.ols$JBT>=0.05),] %>% nrow->j.ols
wntesting.ols[which(wntesting.ols$JBT>=0.05&wntesting.ols$ADF<=0.05&
                      wntesting.ols$Box>=0.05),] %>% nrow->wn.ols
#|---- PAM ----
lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    if(length(all.mods.pols[[z]][[x]])<=2){bt<-NA}
    else{
    all.mods.pols[[z]][[x]]$residuals->res
    Box.test(res)$p.value->bt
    }
    })
})->box.pols
rbindlist(box.pols)->box.pols
names(box.pols) <-as.character(prekes)
box.pols$Comm1<-as.character(prekes)
box.pols %>% gather("Comm2", "Box", 1:18)->box.pols
box.pols[box.pols$Comm1<box.pols$Comm2,]->box.pols
box.pols$Box %>% round(digits = 3)->box.pols$Box

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    if (z==x){ adf<- NA}
    else{ all.mods.pols[[z]][[x]]$residuals->res
      if(length(res)>6){
        adf.test(res)$p.value->adf}
      else {
        adf<- NA
      }
    }
  })
})->adf.pols
rbindlist(adf.pols)->adf.2
names(adf.2) <-as.character(prekes)
adf.2$Comm1<-as.character(prekes)
adf.2 %>% gather("Comm2", "ADF", 1:18)->adf.2
adf.2[adf.2$Comm1<adf.2$Comm2,]->adf.2
adf.2$ADF %>% round(digits = 3)->adf.2$ADF

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    all.mods.pols[[z]][[x]]$residuals->res
    jarque.bera.test(res)$p.value->bt
  })
})->jbt.pols
rbindlist(jbt.pols)->jbt.pols
names(jbt.pols) <-as.character(prekes)
jbt.pols$Comm1<-as.character(prekes)
jbt.pols %>% gather("Comm2", "JBT", 1:18)->jbt.pols
jbt.pols[jbt.pols$Comm1<jbt.pols$Comm2,]->jbt.pols
jbt.pols$JBT %>% round(digits = 3)->jbt.pols$JBT

box.pols %>% full_join(jbt.pols, by = c("Comm1", "Comm2")) %>% 
  full_join(adf.2, by = c("Comm1", "Comm2"))->wntesting.pols
wntesting.pols[which(wntesting.pols$Box>=0.05),] %>% nrow->b.pols
wntesting.pols[which(wntesting.pols$ADF<=0.05),] %>% nrow->a.pols
wntesting.pols[which(wntesting.pols$JBT>=0.05),] %>% nrow->j.pols
wntesting.pols[which(wntesting.pols$JBT>=0.05&wntesting.pols$ADF<=0.05&
                      wntesting.pols$Box>=0.05),] %>% nrow->wn.pols
#|---- ECM ----
lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    if(length(all.mods.ecm[[z]][[x]])<=2){bt<-NA}
    else{
      all.mods.ecm[[z]][[x]]$residuals->res
      Box.test(res)$p.value->bt
    }
  })
})->box.ecm
rbindlist(box.ecm)->box.ecm
names(box.ecm) <-as.character(prekes)
box.ecm$Comm1<-as.character(prekes)
box.ecm %>% gather("Comm2", "Box", 1:18)->box.ecm
box.ecm[box.ecm$Comm1<box.ecm$Comm2,]->box.ecm
box.ecm$Box %>% round(digits = 3)->box.ecm$Box

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    if (z==x){ adf<- NA}
    else{ all.mods.ecm[[z]][[x]]$residuals->res
      if(length(res)>6){
        adf.test(res)$p.value->adf}
      else {
        adf<- NA
      }
    }
  })
})->adf.ecm
rbindlist(adf.ecm)->adf.3
names(adf.3) <-as.character(prekes)
adf.3$Comm1<-as.character(prekes)
adf.3 %>% gather("Comm2", "ADF", 1:18)->adf.3
adf.3[adf.3$Comm1<adf.3$Comm2,]->adf.3
adf.3$ADF %>% round(digits = 3)->adf.3$ADF

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    all.mods.ecm[[z]][[x]]$residuals->res
    jarque.bera.test(res)$p.value->bt
  })
})->jbt.ecm
rbindlist(jbt.ecm)->jbt.ecm
names(jbt.ecm) <-as.character(prekes)
jbt.ecm$Comm1<-as.character(prekes)
jbt.ecm %>% gather("Comm2", "JBT", 1:18)->jbt.ecm
jbt.ecm[jbt.ecm$Comm1<jbt.ecm$Comm2,]->jbt.ecm
jbt.ecm$JBT %>% round(digits = 3)->jbt.ecm$JBT

box.ecm %>% full_join(jbt.ecm, by = c("Comm1", "Comm2")) %>% 
  full_join(adf.3, by = c("Comm1", "Comm2"))->wntesting.ecm
wntesting.ecm[which(wntesting.ecm$Box>=0.05),] %>% nrow->b.ecm
wntesting.ecm[which(wntesting.ecm$ADF<=0.05),] %>% nrow->a.ecm
wntesting.ecm[which(wntesting.ecm$JBT>=0.05),] %>% nrow->j.ecm
wntesting.ecm[which(wntesting.ecm$JBT>=0.05&wntesting.ecm$ADF<=0.05&
                       wntesting.ecm$Box>=0.05),] %>% nrow->wn.ecm

lapply(1:length(prekes), function(z){
  lapply(1:length(prekes), function(x){
    all.mods.ecm[[z]][[x]]$residuals->res
    auto.arima(res)$arma->arma
  })
})->arima.ecm

